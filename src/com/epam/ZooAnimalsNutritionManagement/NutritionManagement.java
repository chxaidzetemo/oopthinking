package com.epam.ZooAnimalsNutritionManagement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NutritionManagement implements Management {

    Map<Animal,List<Nutrition>> animalNutrition = new HashMap<>();

    @Override
    public void addAnimalNutrition(Animal animal, Nutrition nutrition) {
        List<Nutrition> nutritions= animalNutrition.get(animal);

        if(nutritions==null){
            nutritions = new ArrayList<>();
        }
        nutritions.add(nutrition);


        animalNutrition.put(animal, nutritions);
    }

    @Override
    public Map<Animal, List<Nutrition>> getAllAnimalNutritions() {
        return animalNutrition;
    }

    @Override
    public List<Nutrition> getAnimalNutriotions(Animal animal){
        return animalNutrition.get(animal);
    }


}
