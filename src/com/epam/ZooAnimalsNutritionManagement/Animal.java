package com.epam.ZooAnimalsNutritionManagement;


public abstract class Animal {

    @Override
    public String toString() {
        return "Animal{" +
                "name='" + this.getClass().getSimpleName() + '\'' +
                '}';
    }
}
