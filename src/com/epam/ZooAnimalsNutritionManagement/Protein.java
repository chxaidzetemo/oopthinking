package com.epam.ZooAnimalsNutritionManagement;

import java.util.Date;

public class Protein extends Nutrition {

    public Protein(int dosage, Date expirationDate) {
        super(dosage, expirationDate);
    }
}
