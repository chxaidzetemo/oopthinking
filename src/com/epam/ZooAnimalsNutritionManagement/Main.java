package com.epam.ZooAnimalsNutritionManagement;

import java.text.ParseException;
import java.text.SimpleDateFormat;


public class Main {
    public static void main(String[] args) throws ParseException {
        Management nutritionManagement = new NutritionManagement();
        Nutrition protein = new Protein(1,new SimpleDateFormat("dd/MM/yyyy").parse("31/12/2022"));
        Nutrition mineral = new Mineral(2,new SimpleDateFormat("dd/MM/yyyy").parse("10/10/2020"));
        Animal zebra = new Zebra();

        nutritionManagement.addAnimalNutrition(zebra,mineral);
        nutritionManagement.addAnimalNutrition(zebra,protein);

        nutritionManagement.addAnimalNutrition(new Gorilla(),protein);

        System.out.println(nutritionManagement.getAllAnimalNutritions().toString());
//        List<Nutrition> nutriotions = nutritionManagement.getAnimalNutriotions(zebra);
//        System.out.println(nutriotions.toString());


    }
}
