package com.epam.ZooAnimalsNutritionManagement;

import java.util.List;
import java.util.Map;

public interface Management {

    void addAnimalNutrition(Animal animal, Nutrition mineral);

    Map<Animal, List<Nutrition>> getAllAnimalNutritions();

    List<Nutrition> getAnimalNutriotions(Animal animal);


}
