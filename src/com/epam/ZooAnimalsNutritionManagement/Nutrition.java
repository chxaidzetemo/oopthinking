package com.epam.ZooAnimalsNutritionManagement;

import java.util.Date;

public abstract class Nutrition {
    int dosage;
    Date expirationDate;

    public Nutrition(int dosage, Date expirationDate) {
        this.dosage = dosage;
        this.expirationDate = expirationDate;
    }

    @Override
    public String toString() {
        return "Nutrion{" +
                "name='" + this.getClass().getSimpleName() + '\'' +
                ", dosage=" + dosage +
                ", expirationDate=" + expirationDate +
                '}';
    }
}
